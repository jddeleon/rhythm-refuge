class CreateArtistsTracks < ActiveRecord::Migration
  def change
    create_table :artists_tracks, :id => false do |t|
      t.integer :artist_id
      t.integer :track_id
    end
  end
end
