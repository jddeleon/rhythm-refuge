class ChangePriceToFloat < ActiveRecord::Migration
  def up
    change_column :tracks, :price, :decimal, :precision => 8, :scale => 2
  end

  def down
    change_column :tracks, :price, :string
  end
end
