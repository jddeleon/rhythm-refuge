class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :first_name
      t.string :last_name
      t.string :cc_type
      t.string :cc_number
      t.string :exp_month
      t.string :exp_year
      t.string :addr_1
      t.string :addr_2
      t.string :city
      t.string :zip
      t.string :state
      t.references :user, index: true

      t.timestamps
    end
  end
end
