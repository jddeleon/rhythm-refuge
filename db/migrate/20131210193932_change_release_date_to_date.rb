class ChangeReleaseDateToDate < ActiveRecord::Migration
  def up
    change_column :tracks, :release_date, :date
  end

  def down
    change_column :tracks, :release_date, :string
  end
end
