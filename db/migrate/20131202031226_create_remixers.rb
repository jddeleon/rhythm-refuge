class CreateRemixers < ActiveRecord::Migration
  def change
    create_table :remixers do |t|
      t.integer :artist_id

      t.timestamps
    end
  end
end
