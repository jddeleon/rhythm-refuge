class CreateRemixersTracks < ActiveRecord::Migration
  def change
    create_table :remixers_tracks, :id => false do |t|
      t.integer :artist_id
      t.integer :track_id
    end
  end
end
