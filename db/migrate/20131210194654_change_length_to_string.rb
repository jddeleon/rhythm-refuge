class ChangeLengthToString < ActiveRecord::Migration
  def up
    change_column :tracks, :length, :string
  end

  def down
    change_column :tracks, :length, :text
  end
end
