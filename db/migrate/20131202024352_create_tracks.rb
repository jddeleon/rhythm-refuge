class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.string :title
      t.string :genre
      t.text :length
      t.integer :bpm
      t.string :key
      t.text :release_date
      t.string :price

      t.timestamps
    end
  end
end
