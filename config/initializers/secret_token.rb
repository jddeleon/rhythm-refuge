# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
RhythmRefuge::Application.config.secret_key_base = '7ec99b21df2414ba955bd95887d05203021642b35d9e2763d304797599de1f27c20533f39a833d49230af08f804510c7d7f1f8b2777c77ef3ce85cabcb7a43de'
