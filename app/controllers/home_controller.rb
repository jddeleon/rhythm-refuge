class HomeController < ApplicationController
  def index
    @artists = Artist.find(:all).sample(5)
    @djs = Artist.find(:all).sample(3)
    @tracks = Track.find(:all).sample(10)
  end
  
  # Search Track or Artist from search Bar 
  def query
    @tracks = Track.query(params[:query])
    @artists = Artist.query(params[:query])
    
    if !(@tracks.empty? && @artists.empty?)
      flash[:notice] = "Showing search results for \"#{params[:query]}\""
    else
      flash[:alert] = "No results were found for \"#{params[:query]}\""
    end
  end

  def list_by_genre
    @tracks = Track.where(genre: params[:genre])
  end
end
