class TracksController < ApplicationController
  before_action :set_track, only: [:show, :edit, :update, :destroy]
  
  # GET /tracks
  # GET /tracks.json
  def index
    @tracks = Track.all
  end

  # GET /tracks/1
  # GET /tracks/1.json
  def show
  end

  # GET /tracks/new
  def new
    if user_signed_in? && current_user.admin?
      @track = Track.new
      @artists = Artist.all
    elsif user_signed_in? && !current_user.admin?
      redirect_to root_path
    else # user not signed in
      redirect_to new_user_session_path
    end
  end

  # GET /tracks/1/edit
  def edit
    if user_signed_in? && current_user.admin?
      @artists = Artist.all
    elsif user_signed_in? && !current_user.admin?
      redirect_to root_path
    else # user not signed in
      redirect_to new_user_session_path
    end
  end

  # POST /tracks
  # POST /tracks.json
  def create
    if user_signed_in? && current_user.admin?
      @track = Track.new(track_params)
      @artists = Artist.where(:id => params[:artists])
      @track.artists << @artists

      respond_to do |format|
        if @track.save
          format.html { redirect_to @track, notice: 'Track was successfully created.' }
          format.json { render action: 'show', status: :created, location: @track }
        else
          format.html { render action: 'new' }
          format.json { render json: @track.errors, status: :unprocessable_entity }
        end
      end
    elsif user_signed_in? && !current_user.admin?
      redirect_to root_path
    else 
      redirect_to new_user_session_path
    end
  end

  # PATCH/PUT /tracks/1
  # PATCH/PUT /tracks/1.json
  def update
    respond_to do |format|
      if @track.update(track_params)
        format.html { redirect_to @track, notice: 'Track was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @track.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tracks/1
  # DELETE /tracks/1.json
  def destroy
    @track.destroy
    respond_to do |format|
      format.html { redirect_to tracks_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_track
      @track = Track.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def track_params
      params.require(:track).permit(:title, :genre, :length, :bpm, :key, :release_date, :price, :audio)
    end
end
