class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :cart, :dependent => :destroy
  has_many :orders

  validates :fname, :lname, presence: true
  before_save {self.fname = fname.capitalize}
  before_save {self.lname = lname.capitalize}
  after_create :create_cart
  after_create :make_first_user_admin

  def create_cart
    cart = Cart.new
    cart.user_id = self.id
    cart.save
  end

  def make_first_user_admin
    if self.id == 1
      self.update_attribute(:admin, true)
    end
  end

end
