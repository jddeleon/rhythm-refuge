class Order < ActiveRecord::Base
  belongs_to :user
  has_many :line_items

  validates :first_name, :last_name, :cc_type, :cc_number, :exp_month, :exp_year, :addr_1, :city, :zip, :state, :presence => true
  

  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil
      self.line_items << item
    end
  end
end
