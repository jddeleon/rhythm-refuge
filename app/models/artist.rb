class Artist < ActiveRecord::Base
  mount_uploader :picture, ImageUploader
  has_and_belongs_to_many :tracks

  validates :name, presence: true
  def self.query name
    if name
      find(:all, :conditions => ['name LIKE ?', "%#{name}%"])
    else
      find(:all)
    end
  end
end
