class Track < ActiveRecord::Base
  mount_uploader :audio, AudioUploader
  
  has_and_belongs_to_many :artists
  has_and_belongs_to_many :remixers
  has_many :line_items
  
  before_destroy :ensure_not_referenced_by_any_line_item

  validates :title, :genre, :length, :bpm, :key,
            :release_date, :price, presence: true

  def ensure_not_referenced_by_any_line_item
    if line_items.empty?
      return true
    else
      errors.add(:base, 'Line Items present')
      return false
    end
  end
  
  def self.query trkTitle
    puts "**********INSIDE MODEL TRACK******* #{trkTitle}"
    if trkTitle
      find(:all, :conditions => ['title LIKE ?', "%#{trkTitle}%"])
    else
      find(:all)
    end
  end

end
