json.array!(@orders) do |order|
  json.extract! order, :first_name, :last_name, :cc_type, :cc_number, :exp_month, :exp_year, :addr_1, :addr_2, :city, :zip, :state, :user_id
  json.url order_url(order, format: :json)
end
