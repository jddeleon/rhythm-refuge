json.array!(@artists) do |artist|
  json.extract! artist, :name, :bio, :picture
  json.url artist_url(artist, format: :json)
end
