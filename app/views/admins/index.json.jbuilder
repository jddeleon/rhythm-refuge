json.array!(@admins) do |admin|
  json.extract! admin, :fname, :lname
  json.url admin_url(admin, format: :json)
end
