json.array!(@remixers) do |remixer|
  json.extract! remixer, :artist_id
  json.url remixer_url(remixer, format: :json)
end
