json.array!(@tracks) do |track|
  json.extract! track, :title, :genre, :length, :bpm, :key, :release_date, :price
  json.url track_url(track, format: :json)
end
